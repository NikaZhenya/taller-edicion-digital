// Muestra la ventana para descargar el programa en EPUB o PDF
function programa_abrir () {
	var sombra = document.createElement("div"),
		contenedor = document.createElement("div");
		epub = document.createElement("a"),
		pdf = document.createElement("a");

	// Común a epub y pdf
	function enlace_caracteristicas (e) {
		e.target = "_blank";
		e.classList.add("boton");
		e == epub ? e.style.margin = "auto 1em auto -1em" : e.style.marginLeft = "1em";
		e.addEventListener("click", programa_cerrar);
		contenedor.appendChild(e);
	}

	// Contenedor principal
	sombra.id = "sombra";
	sombra.style.position = "fixed";
	sombra.style.top = "0px";
	sombra.style.left = "0px";
	sombra.style.width = "100vw";
	sombra.style.height = "100vh";
	sombra.style.zIndex = "999";
	sombra.style.backgroundColor = "rgba(0,0,0,.5)";

	// Contenedor para los botones
	contenedor.style.display = "flex";
	contenedor.style.width = "100vw";
	contenedor.style.height = "100vh";
	contenedor.style.alignItems = "center";
	contenedor.style.justifyContent = "center";
	contenedor.addEventListener("click", programa_cerrar);

	epub.href = "recursos/programa/programa.epub";
	epub.innerHTML = "EPUB"
	enlace_caracteristicas(epub);
	
	pdf.href = "recursos/programa/programa.pdf";
	pdf.innerHTML = "PDF"
	enlace_caracteristicas(pdf);

	// Se agregan al documento
	sombra.appendChild(contenedor);
	document.body.appendChild(sombra);
}

// Elimina la ventana para descargar el programa
function programa_cerrar () {
	if (document.getElementById("sombra") != null)
		document.body.removeChild(document.getElementById("sombra"));
}

// Funciones cuando se carga la página
window.onload = function () {
	var escena = document.getElementById("escena"),
		paralaje = new Parallax(escena);
};
