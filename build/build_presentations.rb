#!/usr/bin/env ruby
# encoding: UTF-8
# coding: UTF-8

require 'fileutils'

Encoding.default_internal = Encoding::UTF_8

# Función que crea el listado HTML
def build_list
  html = []
  space = '    '

  html.push((space * 4) + '<!-- presentations -->')
  if $htmls.length > 0
    html.push((space * 4) + '<h2>Presentaciones</h2>')
    html.push((space * 4) + '<ol>')
    $htmls.each do |h|
      html.push((space * 5) + '<li>')
      html.push((space * 6) + '<p>' + h[1] + '</p>')
      html.push((space * 6) + '<p class="sin-sangria">')
      html.push((space * 7) + '<a href="src/presentations/html/' + h[0] + '">HTML</a>')
      if h[2] != nil
        html.push((space * 7) + '<a href="' + h[2] + '.mp4">MP4</a>')
        html.push((space * 7) + '<a href="' + h[2] + '.webm">WEBM</a>')
      end
      html.push((space * 6) + '</p>')
      html.push((space * 5) + '</li>')
    end
    html.push((space * 4) + '</ol>')
  end
  html.push((space * 4) + '<!-- presentations -->')

  return html
end

# Variables
$htmls = []

# Va a la carpeta de notas
Dir.chdir(File.dirname(__FILE__) + '/../src/presentations/html')

# Manda a crear cada publicación
Dir.glob('*.html').each do |file|
  html  = File.read(file).gsub("\n",'')
  title = html.gsub(/^.*?<title>(.*?)<\/title>.*$/, '\1')
  video = html.gsub(/^.*?<!--\s+video:(\S*?)\s+-->.*$/, '\1')

  if video =~ /\s/ then video = nil end

  $htmls.push([File.basename(file), title, video])
end

# Ordena alfabéticamente
$htmls.sort!

# Va a la carpeta raíz
Dir.chdir('../../..')

puts "===> Modificando main.html…"

main = File.read('main.html')
           .gsub(/ +?<!-- presentations -->(.|\s)*?<!-- presentations -->/, build_list.join("\n"))

archivo = File.new('main.html', 'w:UTF-8')
archivo.puts main
archivo.close
