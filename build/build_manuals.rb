#!/usr/bin/env ruby
# encoding: UTF-8
# coding: UTF-8

require 'fileutils'

Encoding.default_internal = Encoding::UTF_8

# Función que crea los diferentes formatos
def build dir
    puts "===> Trabajando con «#{dir}»…\n\n"
    Dir.chdir(dir)

    # Obtiene título y sinopsis
    meta = File.read('archivos-madre/meta').split("\n")

    # Crea la portada
    cover = File.read('../template/portada.svg')
                .gsub('$$title$$', meta[0])
    file = File.new('archivos-madre/img/portada.svg', 'w:UTF-8')
    file.puts cover
    file.close
    quiet = `inkscape -z -e archivos-madre/img/portada.png archivos-madre/img/portada.svg`
    FileUtils.rm('archivos-madre/img/portada.svg')

    # Crea el proyecto de automata
    system("pc-automata --init")
    Dir.chdir('epub-automata')

    # Guarda el nombre del directorio para luego modificar el main.html
    $manuals.push([dir, meta[0]])

    # Recrea los metadatos
    archivo = File.new('meta-data.yaml', 'w:UTF-8')
    archivo.puts $meta_data.gsub('$$title$$', meta[0]).gsub('$$synopsis$$', '"' + meta[1] + '"')
    archivo.close

    # Crea los archivos
    system("pc-automata -f ../archivos-madre/md/#{dir}.md #{File.file?('../archivos-madre/md/notas.md') ? '-n ../archivos-madre/md/notas.md' : ''} -c ../archivos-madre/img/portada.png -i ../archivos-madre/img/ --section --no-ace --no-analytics --no-legacy --overwrite")

    # Cambio los nombres
    Dir.glob('*.{epub,mobi}') do |file|
      FileUtils.mv(file, '../' + dir + File.extname(file))
    end

    # Va a la carpeta de MD para crear los HTML
    Dir.chdir('../archivos-madre/md')

    # Genera el HTML con las notas
    system("pc-pandog -i #{dir}.md -o #{dir}.html")
    if File.file?('notas.md') == true
      system("pc-notes -n notas.md --inner")
    end

    html = File.read(dir + '.html')

    # Genera el TOC para el HTML
    headers = ["<h2>Índice</h2>"]

    html.split(/<h1\s*/).each do |l|
      if l =~ /^id/
        clean = l.split('</h1>').first
        id    = clean.split('"')[1]
        tit   = clean.split('"')[2][1..-1]

        headers.push('            <p class="sin-sangria"><a href="#' + id + '">' + tit + '</a></p>')
      end
    end

    headers.push('            <hr class="espacio-arriba3" />')

    # Cambios particulares para el documento HTML
    new_html = html.gsub('<title>Título</title>', "<title>#{meta[0]}</title>")
                   .gsub('<section epub:type="introduction" role="doc-introduction">', "<section><h1 class=\"titulo\">#{meta[0]}</h1></section>\n        <section epub:type=\"toc\" role=\"doc-toc\">\n        #{headers.join("\n")}\n        </section>\n        <section epub:type=\"introduction\" role=\"doc-introduction\">")
                   .gsub('    </body>', "        <section>\n        </section>\n    </body>")
                   .split("\n")

    # Guardado del documento HTML
    file = File.new(dir + '.html', 'w:UTF-8')
    file.puts new_html
    file.close

    # Mueve el HTML
    FileUtils.mv(dir + '.html', '../html')

    # Elimina protecto de automata
    Dir.chdir('../..')
    FileUtils.rm_rf('epub-automata')
    Dir.chdir('..')
end

# Variables
$manuals = []
$meta_data = "---\n# Generales\ntitle: $$title$$\nsubtitle:\nauthor:\n  - Zhenya, Nika\npublisher:\n  - Perro Tuerto\nsynopsis: $$synopsis$$\ncategory: \n  - Edición\n  - Publicaciones\nlanguage: es\nversion: 1.0.0\ncover: portada.png\nnavigation: nav.xhtml\n\n# Tabla de contenidos\nno-toc: \n  - /00(1|2)/\nno-spine: \ncustom: \n\n# Si se quiere EPUB fijo\npx-width: \npx-height: \n\n# Fallbacks\nfallback: \n\n# WCAG:\nsummary: Este EPUB está optimizado para personas con deficiencias visuales; cualquier observación por favor póngase en contacto.\nmode:\n  - textual\n  - visual\nmode-sufficient:\n  - textual, visual\n  - textual\nfeature:\n  - structuralNavigation\n  - alternativeText\n  - resizeText\nhazard:\n  - none\ncontrol:\napi: ARIA"

# Va a la carpeta de manuales
Dir.chdir(File.dirname(__FILE__) + '/../src/manuals')

# Manda a crear cada publicación
Dir.glob('*').each do |dir|
  if dir =~ /^\d/ then build dir end
end

# Ordena los metadatos
$manuals = $manuals.sort

# Va a la carpeta raíz
Dir.chdir('../..')

puts "===> Modificando main.html…"

main = []
write = true
added = false

# Modifica los enlaces a los manuales del main.html
archivo = File.open('main.html', 'r:UTF-8')
archivo.each do |linea|
    if linea =~ /<!-- manuals -->/
        write = !write
    end

    if write
        main.push(linea)
    else
        if !added
            main.push('                <!-- manuals -->')
            main.push('                <ol>')
            $manuals.each do |manual|
                main.push('                    <li>')
                main.push("                        <p><i>#{manual[1]}</i></p>")
                main.push("                        <p class=\"sin-sangria\"><a href=\"src/manuals/#{manual[0]}/archivos-madre/html/#{manual[0]}.html\">HTML</a><a href=\"src/manuals/#{manual[0]}/#{manual[0]}.epub\">EPUB</a><a href=\"src/manuals/#{manual[0]}/#{manual[0]}.mobi\">MOBI</a><a href=\"src/manuals/#{manual[0]}/archivos-madre/md/#{manual[0]}.md\">MD</a></p>")
                main.push('                    </li>')
            end
            main.push('                </ol>')

            added = !added
        end
    end
end
archivo.close
archivo = File.new('main.html', 'w:UTF-8')
archivo.puts main
archivo.close
