#!/usr/bin/env ruby
# encoding: UTF-8
# coding: UTF-8

require 'fileutils'

Encoding.default_internal = Encoding::UTF_8

# Función que crea el listado HTML
def build_list
  html = []
  space = '    '

  html.push((space * 4) + '<!-- videobash -->')
  if $htmls.length > 0
    html.push((space * 4) + '<h2>Videobash</h2>')
    html.push((space * 4) + '<ol>')
    $htmls.each do |h|
      html.push((space * 5) + '<li>')
      html.push((space * 6) + '<p>' + h[1] + '</p>')
      html.push((space * 6) + '<p class="sin-sangria"><a href="src/videobash/html/' + h[0] + '">HTML</a></p>')
      html.push((space * 5) + '</li>')
    end
    html.push((space * 4) + '</ol>')
  end
  html.push((space * 4) + '<!-- videobash -->')

  return html
end

# Variables
$htmls = []

# Va a la carpeta de notas
Dir.chdir(File.dirname(__FILE__) + '/../src/videobash/html')

# Manda a crear cada publicación
Dir.glob('*.html').each do |file|
  title = File.read(file)
              .gsub("\n",'')
              .gsub(/^.*?<title>(.*?)<\/title>.*$/, '\1')

  $htmls.push([File.basename(file), title])
end

# Ordena alfabéticamente
$htmls.sort!

# Va a la carpeta raíz
Dir.chdir('../../..')

puts "===> Modificando main.html…"

main = File.read('main.html')
           .gsub(/ +?<!-- videobash -->(.|\s)*?<!-- videobash -->/, build_list.join("\n"))

archivo = File.new('main.html', 'w:UTF-8')
archivo.puts main
archivo.close
