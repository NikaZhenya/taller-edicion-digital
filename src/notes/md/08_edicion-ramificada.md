<section epub:type="chapter" role="doc-chapter">

# Edición ramificada como alternativa de publicación

¿Y si cambiamos de perspectiva? ¿Si en lugar de hablar sobre la 
diversidad en los formatos finales, pensamos en un método multilateral? La idea 
es sencilla: no nos concentremos en los formatos, sino en los caminos que 
llevan a ellos. El supuesto también es evidente: a múltiples formatos, diversos 
senderos.

![Esquema de la metdología de edición ramificada como alternativa a la edición tradicional y cíclica.](../img/s08_img01.jpg)

## Recursos

1. «[Edición cíclica y edición ramificada: del surgimiento al tronco y las ramas (2/3)](https://marianaeguaras.com/edicion-ciclica-y-edicion-ramificada-del-surgimiento-al-tronco-y-las-ramas/)»
2. «[¿Single source y online publishing, fuera de la realidad?](http://marianaeguaras.com/single-source-online-publishing-la-realidad/)»

----

## SSP como su origen

En los noventa, a raíz del crecimiento de desarrolladores de *software* surgió
el problema de la documentación: el desarrollador tenía la obligación de
documentar sus programas para que este, futuros colaboradores o sus usuarios
aprendieran a utilizar sus herramientas.

El campo de la documentación de *software* tiene dos grandes necesidades:

1. La diversidad de preferencias de soportes y formatos por parte de sus lectores.
2. Las distintas características de *hardware* de cada uno de sus usuarios.

Pronto se observó que las metodologías de publicación tradicionales
no eran aptas para satisfacer sus objetivos por lo que nació la idea del
*single source publishing*: SSP.

El SSP busca:

* Desarrollo *multilateral* de diversos formatos.
* Pretensión de un alto grado de automatización.
* Formatos finales desechables.
* Publicación al instante.

Más de **veinte años** después, recién ahora esta enfoque metodológico
empieza a ser considerado en otros sectores editoriales ajenos a la documentos
de *software*.

Tanto el SSP como la edición ramificada:

* No son un lenguaje.
* No son una solución final.
* No son un entorno de trabajo.
* Sí es una metodología que puede manifestarse de distintas maneras o ajustarse.

## Los 7 elementos de la edición ramificada

![El tronco y las ramas de la edición ramificada.](../img/s08_img02.jpg)

### 1. Archivo madre

Los archivos madre son:

* Los puntos de partida para cualquier formato final.
* Unos documentos con formato abierto.
* Unos archivos con lenguaje de marcado ligero.
* Unos ficheros que obedecen a la estructura del texto.
* Los únicos archivos que —en teoría— no son desechables.
* Los únicos archivos que —en teoría— son los únicos a los que se les mete correcciones.

### 2. De lo simple a lo complejo

Debido a la multiplicidad de formatos potencialmente necesarios,
se vuelve necesario retornar a una noción metodológica básica: partir de lo
simple a lo complejo.

Esto implica que existen formatos más complejos que otros (por ejemplo, el
impreso en comparación al EPUB), por lo que es necesario partir de elementos
comunes básicos para:

* No «ensuciar» un archivo madre con elementos que solo forman parte de un formato.
* Dejar los elementos adicionales en cada formato correspondiente.
* No recurrir directamente al uso de lenguajes «pesados».

### 3. Uso de conversores

Al trabajar con diversos formatos, la conversión automatizada de
documentos permite:

* Disminuir tiempos de desarrollo.
* Disminuir la curva de aprendizaje.
* Evitar errores humanos en la sintaxis.

Sin embargo, también se tienen que tener las siguientes precauciones:

* No es la panacea en la edición.
* Es conflictivo si se utiliza para producir formatos finales.
* Requieren de adaptación a la ortotipografía del español.

### 4. Solo para múltiples formatos

Por sus orígenes, la edición ramificada está pensaba para la 
publicación multiformato. Por ello, **de no existir esta necesidad, esta
metodología quizá no sea la más apta**.

Además, al momento de pensar en optar por esta metodología no solo se tiene que
pensar en las necesidades presentes sino en las posibles exigencias a futuro.
Puede darse el caso que hoy en día solo se desee mantener un formato, pero si
se piensa una apertura a otros más en el futuro, lo recomendable es ir optando
por este modelo de trabajo.

### 5. Tamaño del equipo

Al no tener que esperar ciclos de producción, es posible la división
del trabajo para la producción paralela de múltiples formatos. En este sentido:

* A mayor células de grupos de trabajo,
* menor tiempo requerido para la producción de una obra.

un ejemplo sencillo es tener tres equipos:

1. El encargado de mantener y editar los archivos madre (editores).
2. El responsable de maquetar la obra para impreso (diseñadores).
3. El asignado para desarrollar la publicación digital (programadores).

### 6. Sincronía, independencia y descentralización

En la edición tradicional o cíclica con regularidad nos encontramos 
con estas características:

* El trabajo es *asincrónico*; es decir, siempre se ha de esperar a que un
  formato anterior esté finalizado.
* Los grupos de trabajo o colaboradores *dependen* uno del otro para culminar
  todos los procesos; por ejemplo, los programadores tienen que esperar a que
  los diseñadores terminen de maquetar el libro, así como su calidad de trabajo
  es proporcional a la calidad en el formato hecho por los diseñadores.
* La producción por lo general está *centrada* en el editor; en otros términos:
  por lo general el editor pretende estar en el control de todos los procesos,
  pese a que en la mayoría de los casos tiene un completo desconocimiento.

Sin embargo, por el modo de trabajo en la edición ramificada, se 
obtienen *posibilidades* distintas:

* El trabajo es *sincrónico*; es decir, una vez tenido los archivos madre,
  ninguna célula requiere esperar a otras para comenzar su trabajo.
* Los colaboradores son *independientes* entre sí; por ejemplo, los 
  programadores pueden trabajar sin importar el grado de avance que tengan los
  diseñadores.
* La producción puede ser *descentrada* del editor; en otro términos, el editor
  solo sería responsable de los archivos madre, por lo que no tendría por qué
  tener injerencia durante la producción de los formatos finales, aunque quizá
  sí en darles su visto bueno.

### 7. Diccionarios y expresiones regulares

En el grueso del mundo editorial se tiene está opinión sobre los
diccionarios o las expresiones regulares:

* Los diccionarios son conflictivos e indeseables.
* Las expresiones regulares son desconocidas o consideradas muy complejas.

Ciertamente los diccionarios:

* No pueden ser un sustituto de la edición.
* No evitan al cien por ciento los errores tipográficos.

Sin embargo, también los diccionarios:

* Pueden ser una gran ayuda durante el trabajo de edición.
* Permiten ver errores que varios ojos pueden pasar por alto.

En realidad el problema con los diccionarios no es entorno a ellos
sino a una funcionalidad que se les ha sido asociada: **el reemplazar todo**.

En cuanto a las expresiones regulares:

* Han de manejarse con cuidado.
* Su grado de fiabilidad depende de la calidad en la estructura.
* Permiten un enorme ahorro de tiempo.
* Dan un mayor control estructural.

> ¿Qué es una expresión regular? En cómputo teórico y teoría de lenguajes 
> formales una expresión regular, también conocida como *regex*, *regexp*​ o 
> expresión racional, es una secuencia de caracteres que forma un patrón de 
> búsqueda, principalmente utilizada para la búsqueda de patrones de cadenas de 
> caracteres u operaciones de sustituciones.
> [Más en Wikipedia](https://es.wikipedia.org/wiki/Expresi%C3%B3n_regular).

## Diferencias entre el SSP y la edición ramificada

Las diferencias entre el SSP y la edición ramificada son relativamente
nimias:

* La edición ramificada busca la automatización, pero el cuidado editorial no
  se desea quedar supeditado a las posibilidades que ofrece el proceso 
  automático de producción.
* La edición ramificada hace relevante el respeto a la mayor cantidad de normas
  editoriales posibles.
* La edición ramificada pretense ser más amigable a usuarios no técnicos.

## Críticas comunes

Por su relativa novedad, la edición cíclica es aún una metodología con
un alto grado de experimentación. Esto provoca cierta suspicacia al implantarla
en ambientes de producción reales.

### 1. Demasiado técnico

* Crítica: la metodología requiere aún de demasiados conocimientos técnicos que
  el editor promedio no tiene o no le interesa aprender.
* Concesión: el estado actual de la metodología aún no es lo suficiente madura
  para el uso descontextualizado por parte del editor.
* Contraargumentación: uno de los principales problemas es que el editor 
  promedio ha estado acostumbrado a tratar visualmente el contenido, por lo que
  un par de meses o años en esta metodología tienen una amplia desventaja ante
  toda una vida profesional bajo metodologías tradicionales o cíclicas; además,
  en realidad la exigencia técnica al editor no es gigantesca, ya que este no
  tiene que involucrarse ya en todos los procesos, únicamente en el cuidado de
  archivos madre.

### 2. No es para editores

* Crítica: esta metodología proviene de un campo distinto de la tradición
  editorial, por lo que su pertinencia es causa de sospecha.
* Concesión: efectivamente esta metodología surge por necesidades que la 
  tradición editorial no toma relevancia.
* Contraargumentación: la necesidad actual del grueso editorial es el 
  multiformato, donde la edición tradicional o cíclica se ve ampliamente 
  limitada; la edición ramifica surge para solventar esta necesidad así como
  evitar los vicios comunes dentro de los procesos tradicionales o cíclicos;
  además, el grueso editorial basa ya su producción mediante la edición digital,
  ¿por qué limitar esto a los «formatos» y «programas», si puede extenderse al
  método que le sirve de fondo?

### 3. Requiere esfuerzo corporativo

* Crítica: no existe un entorno amigable de trabajo, por lo que es necesario
  una inversión por parte de corporaciones para hacer un entorno gráfico 
  accesible.
* Concesión: el trabajo en la edición cíclica aún demanda que el editor, 
  diseñador y programador utilice una amplica gama de programas, que incluso
  su instalación puede ser tortuosa.
* Contraargumentación: al ser un trabajo donado, las personas involucradas en
  esta metodología no pueden dedicarle tiempo completo a su crecimiento; más que
  una desventaja, es una oportunidad para que más personas se sumen o para que
  el usuario, mediante su uso o comentarios, tenga voz durante el desarrollo
  de las diversas herramientas necesarias; esto puede incluso planearse para
  la producción de un **Entorno de Edición Integrada o Entorno de Publicación Integrada**.

</section>
