<section epub:type="chapter" role="doc-chapter">

# El formato como el cuello de botella en la edición

El formateo del texto es una de las tareas que pueden llegar a 
consumir la mayor cantidad de tiempo al momento de editar una publicación. Esto
se debe a que **la calidad de una publicación depende de su formato**. La
relevancia de esto es para cualquier clase de método y, para fines de 
publicación multiformato, es esencial si se quiere evitar la mayor cantidad de
regresiones o bifurcaciones posibles.

![Diagrama del cuello de botella.](../img/s05_img01.jpg)

## Recursos

1. [El formato de una publicación: cuello de botella en la edición](http://marianaeguaras.com/el-formato-de-una-publicacion-cuello-de-botella-en-la-edicion/). 

----

## Formato adecuado

Los requisitos mínimos para un formato adecuado son los siguientes:

* Esté en un lenguaje de marcado.
* El formateo sea previo a la edición o, por lo menos, se realice antes de 
   crear el archivo final.
* Se opte por un formato abierto y estandarizado.
* Tenga un mecanismo de control de versiones, para evitar sobrescrituras 
   indeseadas.
* Los «archivos madre» sean los únicos donde se meten los cambios y los
   archivos finales siempre sean de carácter residual.
* Orden consistente en la estructura del proyecto.

## Dificultades evitadas

Con un cuidado adecuado es posible evitar los siguientes 
inconvenientes:

* La estructura del contenido se mantiene, con independencia del archivo final 
  —EPUB o PDF, por ejemplo—, por lo que se evita ambigüedad en los estilos, 
  como la falta de uniformidad en encabezados, párrafos, etcétera.
* La conversión entre formatos es sencilla y no se presta a descrontrol, lo que 
  permite poder maquetar el texto en el entorno que se prefiera, como puede ser 
  LaTeX, Scribus, InDesign o Sigil, por mencionar algunos.
* El tiempo de publicación en diversos soportes —EPUB, MOBI, IBOOKS, PDF para 
  impresión, etcétera— deja de ser proporcional a la cantidad de archivos 
  deseados; incluso sus tiempos de producción disminuyen a segundos.
* El contenido se conserva a lo largo del tiempo, sin importar el cambio de 
  tendencias o que el *software* utilizado para hacer la publicación deje de 
  tener soporte, evitándose ese dolor de cabeza —y derroche de recursos— de 
  tener que recuperar información o, peor aún, tenerla que rehacer.
* La duplicación innecesaria y varias veces descontrolada de los archivos.
* La ausencia de una línea de tiempo que permita conocer puntualmente cada uno
  de los cambios hechos en el proyecto.

## Posibilidades abiertas

Además del control de la publicación gracias al cuidado del texto,
se abren otras puertas en relación con la obra:

* La conservación se vuelve de muy bajo mantenimiento.
* La experimentación no es conflictiva y su adición al proyecto es sencilla.
* La independencia del *software* utilizado evita el despilfarro de tiempo o
   de recursos para el mantenimiento o la adquisición de programas.
* Se hace innecesario contar con equipo de cómputo de última generación.
* La automatización se facilita.
* El análisis de información también se favorece.

</section>
