window.onload = function () {
  var link = document.createElement( 'link' );
      link.rel = 'stylesheet';
      link.type = 'text/css';
      link.href = window.location.search.match( /print-pdf/gi ) ? '../css/print/pdf.css' : '../css/print/paper.css';
      document.getElementsByTagName( 'head' )[0].appendChild( link );

  Reveal.configure({ pdfMaxPagesPerSlide: 1 });
  Reveal.initialize({
    dependencies: [
      { src: '../plugin/markdown/marked.js' },
      { src: '../plugin/markdown/markdown.js' },
      { src: '../plugin/notes/notes.js', async: true },
      { src: '../plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
    ]
  });
}
